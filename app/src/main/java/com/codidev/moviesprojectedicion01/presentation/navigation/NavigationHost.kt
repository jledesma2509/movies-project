package com.codidev.moviesprojectedicion01.presentation.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.codidev.moviesprojectedicion01.presentation.screens.DetailScreen
import com.codidev.moviesprojectedicion01.presentation.screens.HomeScreen
import com.codidev.moviesprojectedicion01.presentation.screens.SplashScreen

@Composable
fun NavigationHost() {

    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = Destination.SplashScreen.route){

        composable(Destination.SplashScreen.route){
            SplashScreen(navController = navController)
        }
        composable(Destination.HomeScreen.route){
            HomeScreen()
        }
        composable(Destination.DetailScreen.route){
            DetailScreen()
        }

    }


}