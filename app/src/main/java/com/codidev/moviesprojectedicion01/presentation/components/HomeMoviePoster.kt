package com.codidev.moviesprojectedicion01.presentation.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.request.ImageRequest
import com.codidev.moviesprojectedicion01.R

@Composable
fun HomeMoviePoster(
    imageUrl: String,
    modifier: Modifier = Modifier,
    onMovieClick: () -> Unit
) {

    AsyncImage(
        model = ImageRequest
            .Builder(LocalContext.current)
            .data(imageUrl)
            .crossfade(2000)
            .build(),
        contentDescription = stringResource(R.string.poster),
        modifier = modifier.clip(RoundedCornerShape(8.dp))
            .size(width = 138.dp, height = 180.dp).clickable {
                onMovieClick()
            },
        contentScale = ContentScale.FillBounds
    )

}