package com.codidev.moviesprojectedicion01.presentation.screens.Home

import com.codidev.moviesprojectedicion01.data.FilterType
import com.codidev.moviesprojectedicion01.data.MovieRemote
import com.codidev.moviesprojectedicion01.domain.Movie

data class HomeState(
   val isLoading : Boolean = false,
   val upcomingMovies: List<Movie> = emptyList(),
   val popularMovies : List<Movie> = emptyList(),
   val selectedFilter : FilterType = FilterType.SPANISH,
   val filterMovies : List<Movie> = emptyList(),
   val error:String? = null
)
