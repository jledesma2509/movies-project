package com.codidev.moviesprojectedicion01.presentation.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.codidev.moviesprojectedicion01.R

@Composable
fun Header(modifier: Modifier = Modifier) {

    Image(
        painterResource(id = R.drawable.logo),
        contentDescription = stringResource(R.string.app_name),
        modifier = modifier
            .fillMaxWidth()
            .padding(16.dp)
    )


}

