package com.codidev.moviesprojectedicion01.presentation.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.codidev.moviesprojectedicion01.presentation.components.Header
import com.codidev.moviesprojectedicion01.presentation.navigation.Destination
import com.codidev.moviesprojectedicion01.ui.theme.Background_main
import com.codidev.moviesprojectedicion01.ui.theme.Background_secundary
import kotlinx.coroutines.delay

@Composable
fun SplashScreen(navController: NavHostController) {

    LaunchedEffect(key1 = true){
        delay(3000)
        navController.popBackStack()
        navController.navigate(Destination.HomeScreen.route)
    }


    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(
                brush = Brush.verticalGradient(
                    colors = listOf(
                        Background_main,
                        Background_secundary
                    )
                )
            ), contentAlignment = Alignment.Center
    ) {
        Header(modifier = Modifier.height(80.dp))
    }


}