package com.codidev.moviesprojectedicion01.presentation.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.codidev.moviesprojectedicion01.ui.theme.Background_secundary
import com.codidev.moviesprojectedicion01.R
import com.codidev.moviesprojectedicion01.presentation.components.*
import com.codidev.moviesprojectedicion01.presentation.screens.Home.HomeEvent
import com.codidev.moviesprojectedicion01.presentation.screens.Home.HomeViewModel

@Composable
fun HomeScreen(modifier: Modifier = Modifier, viewModel: HomeViewModel = hiltViewModel()) {

    val state = viewModel.state
    val scrollState = rememberScrollState()

    /*Column(
        modifier = modifier
            .fillMaxSize()
            .background(color = Background_secundary)
            .padding(start = 16.dp)
            .verticalScroll(scrollState)
    ) {

        Header(modifier = Modifier.height(60.dp))

        if (state.upcomingMovies.isNotEmpty()) {
            HomeMovieList(
                stringResource(id = R.string.upcoming_releases),
                movies = state.upcomingMovies
            ) {
            }
        }

        Divider(modifier = Modifier.height(8.dp))

        if (state.upcomingMovies.isNotEmpty()) {
            HomeMovieList(
                stringResource(id = R.string.popular),
                movies = state.popularMovies
            ) {
            }
        }

        Divider(modifier = Modifier.height(8.dp))

        HomeRecommended(
            selectedFilter = state.selectedFilter , onFilterClick = {

            }
        )

        HomeGridList(
            movies = state.filterMovies
        ){

        }


    }*/


    LazyVerticalGrid(
        columns = GridCells.Fixed(2),
        verticalArrangement = Arrangement.spacedBy(24.dp),
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black)
            .padding(start = 24.dp, end = 24.dp)
    ) {

        item(span = {
            GridItemSpan(2)
        }) {
            Header(modifier = Modifier.height(60.dp))
        }

        if (state.upcomingMovies.isNotEmpty()) {

            item(span ={
                GridItemSpan(2)
            }){
                HomeMovieList(
                    stringResource(id = R.string.upcoming_releases),
                    movies = state.upcomingMovies
                ) {
                }
            }
        }

        if (state.popularMovies.isNotEmpty()) {

            item(span ={
                GridItemSpan(2)
            }){
                HomeMovieList(
                    stringResource(id = R.string.popular),
                    movies = state.popularMovies
                ) {
                    
                }
            }
        }

        item(span ={
            GridItemSpan(2)
        }){
            HomeRecommended(
                selectedFilter = state.selectedFilter , onFilterClick = {
                    viewModel.onEvent(HomeEvent.ChangeFilter(it))
                }
            )
        }

        items(state.filterMovies){
            HomeMoviePoster(imageUrl = it.poster) {

            }
        }
    }


    if (state.isLoading) {
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            CircularProgressIndicator(color = Color.White, strokeWidth = 8.dp)
        }
    }


}