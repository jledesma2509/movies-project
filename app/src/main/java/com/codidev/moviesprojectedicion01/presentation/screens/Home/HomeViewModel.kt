package com.codidev.moviesprojectedicion01.presentation.screens.Home

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.codidev.moviesprojectedicion01.data.FilterType
import com.codidev.moviesprojectedicion01.data.Result
import com.codidev.moviesprojectedicion01.data.repositories.MoviesRepository
import com.codidev.moviesprojectedicion01.domain.usescase.GetMovies
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val repository: MoviesRepository) : ViewModel() {

    var state by mutableStateOf(HomeState())
        private set


    init {
        state = state.copy(isLoading = true)

        viewModelScope.launch {

            supervisorScope {
                /*val upcommingMovies = launch { getUpcomingMovies() }
                val popularMovies = launch { getPopularMovies() }
                val moviesFiltered = launch { getMoviesByFilter() }*/

                val movies = launch { getAllMovies() }

                //listOf(upcommingMovies,popularMovies,moviesFiltered).forEach { it.join() }

                movies.join()

                state = state.copy(isLoading = false)
            }

        }


    }

    private suspend fun getAllMovies() {
        repository.getAllMovies(false,state.selectedFilter).collect() {
            state = state.copy(
                upcomingMovies = it.upcoming,
                popularMovies = it.trending,
                filterMovies = it.filtered
            )
        }
    }

    private suspend fun getMoviesByFilter() {

        when (state.selectedFilter) {
            FilterType.SPANISH -> {
                repository.getMoviesByLanguage("es").collect() {
                    state = state.copy(
                        filterMovies = it.data!!
                    )
                }
            }
            FilterType.NINETY_THREE -> {
                repository.getMoviesByYear(1993).collect() {
                    state = state.copy(
                        filterMovies = it.data!!
                    )
                }
            }
        }

    }

    private suspend fun getPopularMovies() {
        repository.getPopularMovies().collect() {
            when (it) {
                is Result.Error -> {
                    state = state.copy(error = it.message)
                }
                is Result.Success -> {
                    state = state.copy(popularMovies = it.data!!)
                }
            }
        }
    }

    private suspend fun getUpcomingMovies() {
        repository.getUpcommingMovies().collect() {
            when (it) {
                is Result.Error -> {
                    state = state.copy(error = it.message)
                }
                is Result.Success -> {
                    state = state.copy(upcomingMovies = it.data!!)
                }
            }
        }
    }

    fun onEvent(event: HomeEvent) {

        when (event) {
            is HomeEvent.ChangeFilter -> {
                if (event.filterType != state.selectedFilter) {
                    state = state.copy(
                        selectedFilter = event.filterType
                    )
                    viewModelScope.launch {
                        //getMoviesByFilter()

                        repository.getAllMovies(true,state.selectedFilter).collect() {
                            state = state.copy(
                                filterMovies = it.filtered
                            )
                        }

                    }


                }
            }

        }

    }


}