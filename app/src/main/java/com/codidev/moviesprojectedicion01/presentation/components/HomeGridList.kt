package com.codidev.moviesprojectedicion01.presentation.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.codidev.moviesprojectedicion01.domain.Movie

@Composable
fun HomeGridList(
    movies : List<Movie>,
    modifier: Modifier = Modifier,
    onMovieClick:(Movie)->Unit
) {

    Column(modifier = modifier) {
        Spacer(modifier = Modifier.height(16.dp))
        LazyVerticalGrid(
            contentPadding = PaddingValues(2.dp),
            columns = GridCells.Fixed(2)
        ){
            items(movies){
                HomeMoviePoster(imageUrl = it.poster) {
                    onMovieClick(it)
                }
            }
        }

    }


}