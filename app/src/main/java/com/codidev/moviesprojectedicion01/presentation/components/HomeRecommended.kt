package com.codidev.moviesprojectedicion01.presentation.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.codidev.moviesprojectedicion01.R
import com.codidev.moviesprojectedicion01.data.FilterType

@Composable
fun HomeRecommended(
    selectedFilter: FilterType,
    onFilterClick:(FilterType)->Unit,
    modifier: Modifier = Modifier
) {

    Column(modifier = modifier.fillMaxWidth()) {
        CategoryTitle(title = stringResource(R.string.home_recommended))
        Spacer(modifier = Modifier.height(12.dp))
        FilterOptionsContainer(
            filters = FilterType.values().toList(),
            selectedFilter = selectedFilter,
            onFilterClick = onFilterClick
        )

    }
    
}