package com.codidev.moviesprojectedicion01.presentation.navigation

sealed class Destination(val route:String){

    object SplashScreen : Destination("SPLASH")
    object HomeScreen : Destination("HOME")
    object DetailScreen : Destination("DETAIL")

}