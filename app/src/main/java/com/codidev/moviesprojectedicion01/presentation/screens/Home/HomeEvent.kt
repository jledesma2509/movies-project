package com.codidev.moviesprojectedicion01.presentation.screens.Home

import com.codidev.moviesprojectedicion01.data.FilterType

sealed class HomeEvent {
    data class ChangeFilter(val filterType: FilterType) : HomeEvent()
}
