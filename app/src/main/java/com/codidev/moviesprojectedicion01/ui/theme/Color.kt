package com.codidev.moviesprojectedicion01.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val Background_main = Color(0xFF2e1919)
val Background_secundary = Color(0xFF1E1E1E)