package com.codidev.moviesprojectedicion01.data.repositories

import com.codidev.moviesprojectedicion01.data.FilterType
import com.codidev.moviesprojectedicion01.data.database.MovieEntity
import com.codidev.moviesprojectedicion01.data.database.MovieType
import com.codidev.moviesprojectedicion01.data.datasource.MovieLocalDataSource
import com.codidev.moviesprojectedicion01.data.datasource.MovieLocalDataSourceImp
import com.codidev.moviesprojectedicion01.data.datasource.MovieRemoteDataSource
import com.codidev.moviesprojectedicion01.data.toDomain
import com.codidev.moviesprojectedicion01.data.toEntity
import com.codidev.moviesprojectedicion01.domain.Movie
import com.codidev.moviesprojectedicion01.domain.MovieList
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MoviesRepository @Inject constructor(
    private val movieRemoteDataSource : MovieRemoteDataSource,
    private val movieLocalDataSource: MovieLocalDataSource) {

    suspend fun getUpcommingMovies() = flow {
        emit(movieRemoteDataSource.getUpcomingMovies())
    }

    suspend fun getPopularMovies() = flow {
       emit(movieRemoteDataSource.getPopularMovies())
    }

    suspend fun getMoviesByYear(year:Int) = flow {
        emit(movieRemoteDataSource.getMoviesByYear(year))
    }

    suspend fun getMoviesByLanguage(language:String) = flow {
        emit(movieRemoteDataSource.getMoviesByLanguage(language))
    }

    //Base de datos Local
    suspend fun getAllMovies(isFilteredOnly:Boolean, filterType: FilterType) = flow {

        emit(getMovieListLocally(filterType))

        if(!isFilteredOnly) {
            val moviesUpcoming = movieRemoteDataSource.getUpcomingMovies()
            moviesUpcoming.data?.let {
                saveMoviesLocally(it, MovieType.UPCOMING)
                emit(getMovieListLocally(filterType))
            }

            val moviesPopular = movieRemoteDataSource.getPopularMovies()
            moviesPopular.data?.let {
                saveMoviesLocally(it, MovieType.TRENDING)
                emit(getMovieListLocally(filterType))
            }
        }

        val moviesByYear = movieRemoteDataSource.getMoviesByYear(1993)
        moviesByYear.data?.let {
            saveMoviesLocally(it, MovieType.NINETY_THREE)
            emit(getMovieListLocally(filterType))
        }

        val moviesByLanguagePopular = movieRemoteDataSource.getMoviesByLanguage("es")
        moviesByLanguagePopular.data?.let {
            saveMoviesLocally(it, MovieType.SPANISH)
            emit(getMovieListLocally(filterType))
        }


    }

    suspend fun saveMoviesLocally(movies:List<Movie>,movieType:MovieType){
        movies.forEach { movieLocalDataSource.save(it.toEntity(movieType)) }
    }

    suspend fun getMovieListLocally(filterType: FilterType) : MovieList {
        val localMovies = movieLocalDataSource.movies()
        val movieTypeFromFilter = when(filterType){
            FilterType.SPANISH -> MovieType.SPANISH
            FilterType.NINETY_THREE -> MovieType.NINETY_THREE
        }

        val a: List<Movie> = getMapMovies(localMovies,movieTypeFromFilter)
        val b = ""
        return MovieList(
            upcoming = getMapMovies(localMovies,MovieType.UPCOMING),
            trending = getMapMovies(localMovies,MovieType.TRENDING),
            filtered =  getMapMovies(localMovies,movieTypeFromFilter),
        )



    }

    fun getMapMovies(movies:List<MovieEntity>, movieType: MovieType) : List<Movie> {
        return movies.filter { it.type == movieType }.map { it.toDomain() }
    }




}