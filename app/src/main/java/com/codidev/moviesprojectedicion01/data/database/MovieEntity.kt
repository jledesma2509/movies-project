package com.codidev.moviesprojectedicion01.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "MovieEntity")
data class MovieEntity(
    @PrimaryKey
    val id:Int,
    val poster:String,
    val type: MovieType

)

enum class MovieType{
    UPCOMING,
    TRENDING,
    SPANISH,
    NINETY_THREE
}
