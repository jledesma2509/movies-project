package com.codidev.moviesprojectedicion01.data.datasource

import com.codidev.moviesprojectedicion01.data.database.MovieDao
import com.codidev.moviesprojectedicion01.data.database.MovieEntity
import javax.inject.Inject

class MovieLocalDataSourceImp @Inject constructor(private val movieDao: MovieDao) : MovieLocalDataSource {

    override suspend fun save(movieEntity: MovieEntity) {
        movieDao.insertMovie(movieEntity)
    }

    override suspend fun movies(): List<MovieEntity> = movieDao.getMovies()



}