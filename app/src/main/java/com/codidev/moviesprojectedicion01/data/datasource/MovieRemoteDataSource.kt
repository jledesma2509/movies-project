package com.codidev.moviesprojectedicion01.data.datasource

import com.codidev.moviesprojectedicion01.data.Result
import com.codidev.moviesprojectedicion01.domain.Movie

interface MovieRemoteDataSource {

    suspend fun getUpcomingMovies() : Result<List<Movie>>
    suspend fun getPopularMovies() : Result<List<Movie>>
    suspend fun getMoviesByYear(year:Int) :  Result<List<Movie>>
    suspend fun getMoviesByLanguage(language:String) :  Result<List<Movie>>
}