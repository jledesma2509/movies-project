package com.codidev.moviesprojectedicion01.data

enum class FilterType(val text:String) {
    SPANISH("En español"),
    NINETY_THREE("Lanzadas en 1993")
}