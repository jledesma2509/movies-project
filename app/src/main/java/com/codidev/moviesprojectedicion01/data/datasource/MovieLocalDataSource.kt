package com.codidev.moviesprojectedicion01.data.datasource

import com.codidev.moviesprojectedicion01.data.database.MovieEntity

interface MovieLocalDataSource {

    suspend fun save(movieEntity:MovieEntity)
    suspend fun movies():List<MovieEntity>

}