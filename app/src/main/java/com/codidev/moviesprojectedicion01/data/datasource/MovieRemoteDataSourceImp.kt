package com.codidev.moviesprojectedicion01.data.datasource

import coil.network.HttpException
import com.codidev.moviesprojectedicion01.data.RemoteService
import com.codidev.moviesprojectedicion01.data.Result
import com.codidev.moviesprojectedicion01.data.toDomain
import com.codidev.moviesprojectedicion01.domain.Movie
import java.io.IOException
import javax.inject.Inject

class MovieRemoteDataSourceImp @Inject constructor(private val remoteService: RemoteService) : MovieRemoteDataSource {

    override suspend fun getUpcomingMovies(): Result<List<Movie>> {

        return try{
            val response = remoteService.getUpComingMovies()
            Result.Success(data = response.results.map { it.toDomain() })
        }catch (ex:HttpException){
            Result.Error(message = "Oops, something went wrong")
        }catch (ex:IOException){
            Result.Error(message = "Couldn't reach server, check your internet connection")
        }

    }

    override suspend fun getPopularMovies(): Result<List<Movie>> {
        return try{
            val response = remoteService.getPopularMovies()
            Result.Success(data = response.results.map { it.toDomain() })
        }catch (ex:HttpException){
            Result.Error(message = "Oops, something went wrong")
        }catch (ex:IOException){
            Result.Error(message = "Couldn't reach server, check your internet connection")
        }
    }

    override suspend fun getMoviesByYear(year: Int): Result<List<Movie>> {
        return try{
            val response = remoteService.getMoviesByYear(year)
            Result.Success(data = response.results.map { it.toDomain() })
        }catch (ex:HttpException){
            Result.Error(message = "Oops, something went wrong")
        }catch (ex:IOException){
            Result.Error(message = "Couldn't reach server, check your internet connection")
        }
    }

    override suspend fun getMoviesByLanguage(language: String): Result<List<Movie>> {
        return try{
            val response = remoteService.getMoviesByLanguage(language)
            Result.Success(data = response.results.map { it.toDomain() })
        }catch (ex:HttpException){
            Result.Error(message = "Oops, something went wrong")
        }catch (ex:IOException){
            Result.Error(message = "Couldn't reach server, check your internet connection")
        }
    }
}