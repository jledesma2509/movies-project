package com.codidev.moviesprojectedicion01.data

import retrofit2.http.GET
import retrofit2.http.Query

interface RemoteService {

    //https://api.themoviedb.org/3/movie/upcoming?api_key=<<api_key>>

    //RETROFIT
    //URL BASE = https://api.themoviedb.org/3/
    //METODO = movie/upcoming

    @GET("movie/upcoming?api_key=8bb03a1e90b4207d88e6ab811254fe9b")
    suspend fun getUpComingMovies() : MovieRemoteResponse

    @GET("movie/popular?api_key=8bb03a1e90b4207d88e6ab811254fe9b")
    suspend fun getPopularMovies() : MovieRemoteResponse

    @GET("discover/movie?sort_by=popularity.desc&include_adult=false&api_key=8bb03a1e90b4207d88e6ab811254fe9b")
    suspend fun getMoviesByYear(@Query("year") year:Int) : MovieRemoteResponse

    @GET("discover/movie?sort_by=popularity.desc&include_adult=false&api_key=8bb03a1e90b4207d88e6ab811254fe9b")
    suspend fun getMoviesByLanguage(@Query("with_original_language") language:String) : MovieRemoteResponse


}