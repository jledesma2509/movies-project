package com.codidev.moviesprojectedicion01.data

import com.codidev.moviesprojectedicion01.data.database.MovieEntity
import com.codidev.moviesprojectedicion01.data.database.MovieType
import com.codidev.moviesprojectedicion01.domain.Movie

//List<RemoteMovie> -> List<Movie>
fun MovieRemote.toDomain(): Movie{
    return Movie(
        id = this.id,
        poster = "https://image.tmdb.org/t/p/original/" + this.posterPath
    )
}

//Movie -> EntityMovie
fun Movie.toEntity(type : MovieType) : MovieEntity{
    return MovieEntity(
        id = this.id,
        poster = this.poster,
        type = type
    )
}

//EntityMovie -> Movie
fun MovieEntity.toDomain(): Movie{
    return Movie(
        id = this.id,
        poster = this.poster
    )
}