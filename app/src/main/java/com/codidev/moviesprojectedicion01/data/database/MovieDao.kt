package com.codidev.moviesprojectedicion01.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovie(movie:MovieEntity)

    @Query("SELECT *FROM MovieEntity")
    suspend fun getMovies():List<MovieEntity>

}