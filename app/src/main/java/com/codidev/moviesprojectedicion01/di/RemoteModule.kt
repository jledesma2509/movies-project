package com.codidev.moviesprojectedicion01.di

import com.codidev.moviesprojectedicion01.data.RemoteService
import com.codidev.moviesprojectedicion01.data.datasource.MovieLocalDataSource
import com.codidev.moviesprojectedicion01.data.datasource.MovieLocalDataSourceImp
import com.codidev.moviesprojectedicion01.data.datasource.MovieRemoteDataSource
import com.codidev.moviesprojectedicion01.data.datasource.MovieRemoteDataSourceImp
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class RemoteModule {

    @Provides
    @Singleton
    fun provideRemoteService(): RemoteService {
        return Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .addConverterFactory(GsonConverterFactory.create())
            .build().create()
    }

}

@Module
@InstallIn(SingletonComponent::class)
abstract class AppDataModule {

    @Binds
    abstract fun bindMovieRemoteDataSource(movieRemoteDataSourceImp : MovieRemoteDataSourceImp) : MovieRemoteDataSource

    @Binds
    abstract fun bindMovieLocalDataSource(movieLocalDataSourceImp: MovieLocalDataSourceImp) : MovieLocalDataSource

}

