package com.codidev.moviesprojectedicion01.di

import android.content.Context
import androidx.room.Room
import com.codidev.moviesprojectedicion01.data.database.AppDatabase
import com.codidev.moviesprojectedicion01.data.database.MovieDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule{

    @Provides
    @Singleton
    fun provideDatabase(@ApplicationContext context: Context) : AppDatabase = Room.databaseBuilder(
        context,
        AppDatabase::class.java,
        "dbmovies"
    ).build()


    @Provides
    @Singleton
    fun provideDao(db:AppDatabase) : MovieDao = db.movieDao



}