package com.codidev.moviesprojectedicion01.domain

data class MovieList(
   val upcoming : List<Movie>,
   val trending : List<Movie>,
   val filtered : List<Movie>,
)
