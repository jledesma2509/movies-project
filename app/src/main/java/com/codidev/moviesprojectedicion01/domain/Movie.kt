package com.codidev.moviesprojectedicion01.domain

data class Movie(
    val id:Int,
    val poster:String
)